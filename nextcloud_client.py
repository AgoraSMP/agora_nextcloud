# Copyright (C) 2022 AgoraSMP Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import json

import requests
from django.conf import settings
from requests.auth import HTTPBasicAuth


class NextcloudClient:
    def __init__(self):
        self.host = settings.NEXTCLOUD_HOST
        self.user = settings.NEXTCLOUD_USER
        self.passwd = settings.NEXTCLOUD_PASSWORD

    def ocs(self, path, method="GET", data={}):
        fullpath = self.host.rstrip("/") + "/index.php/apps/groupfolders/" + path
        headers = {"OCS-APIRequest": "true", "Accept": "application/json"}
        auth = HTTPBasicAuth(self.user, self.passwd)
        if method == "GET":
            r = requests.get(fullpath, headers=headers, auth=auth)
        elif method == "POST":
            headers["Content-Type"] = "application/x-www-form-urlencoded"
            r = requests.post(fullpath, headers=headers, auth=auth, data=data)
        else:
            raise Exception("unknown method {}".format(method))
        return r.text

    def get_groupfolders(self):
        api_response = self.ocs("folders")
        j = json.loads(api_response)
        folders = {}
        if j["ocs"]["data"]:
            for fid, folder in j["ocs"]["data"].items():
                # TODO maybe we need more than just the mount point
                name = folder["mount_point"]
                folders[int(fid)] = name
        return folders

    def create_groupfolder(self, name, group):
        # create the folder
        data = {"mountpoint": name}
        api_response = self.ocs("folders", method="POST", data=data)
        j = json.loads(api_response)
        fid = int(j["ocs"]["data"]["id"])  # we get a folder ID back

        # bind the folder to the group
        data = {"group": group}
        api_response = self.ocs(
            "folders/{}/groups".format(fid), method="POST", data=data
        )
        j = json.loads(api_response)
        if not j["ocs"]["data"]:
            raise Exception("Group assignment failed")

        # set permissions for that group
        perms = 7  # read, update, create (but not share, delete)
        data = {"permissions": str(perms)}
        api_response = self.ocs(
            "folders/{}/groups/{}".format(fid, group), method="POST", data=data
        )
        j = json.loads(api_response)

        if not j["ocs"]["data"]:
            raise Exception("Permissions assignment failed")

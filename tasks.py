# Copyright (C) 2022 AgoraSMP Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from celery import shared_task
from celery.utils.log import get_task_logger

from agora_nextcloud.nextcloud_client import NextcloudClient

logger = get_task_logger(__name__)

from usermgr.models import Seminar, Workgroup


@shared_task(bind=True, retry_backoff=True, acks_late=True)
def handle_seminar_access_opens(self, seminar=None):
    try:
        seminar = Seminar.objects.get(pk=seminar)
    except Seminar.DoesNotExist:
        logger.error(
            "Tried to open access to seminar with id %s, but Seminar object does not exist (anymore?)",
            seminar,
        )
        return

    client = NextcloudClient()

    name = escape_seminar_name_for_nextcloud_directory_name(seminar.name)
    slug = seminar.slug
    group = "s-" + slug
    workgroups = seminar.workgroup_set.all()

    try:
        folders = client.get_groupfolders().values()
    except Exception as e:
        raise self.retry(exc=e)

    if name not in folders:
        try:
            client.create_groupfolder(name, group)
        except Exception as e:
            raise self.retry(exc=e)

    for wg in workgroups:
        wg_name = escape_workgroup_name_for_nextcloud_directory_name(wg.name)
        if wg_name in folders:
            continue
        wg_group = "w-" + wg.slug
        try:
            client.create_groupfolder(wg_name, wg_group)
        except Exception as e:
            raise self.retry(exc=e)


@shared_task(bind=True, retry_backoff=True, acks_late=True)
def handle_workgroup_access_opens__nonsynthetic(self, workgroup=None):
    try:
        workgroup = Workgroup.objects.get(pk=workgroup)
    except Workgroup.DoesNotExist:
        logger.error(
            "Tried to open access to workgroup with id %s, but Workgroup object does not exist (anymore?)",
            workgroup,
        )
        return

    client = NextcloudClient()

    wg_name = escape_workgroup_name_for_nextcloud_directory_name(workgroup.name)
    wg_slug = workgroup.slug

    group = "w-" + wg_slug
    folders = client.get_groupfolders().values()

    if wg_name in folders:
        return

    client.create_groupfolder(wg_name, group)


def escape_seminar_name_for_nextcloud_directory_name(name):
    return "Seminar " + name.replace("/", "_")


def escape_workgroup_name_for_nextcloud_directory_name(name):
    return "AG " + name.replace("/", "_")
